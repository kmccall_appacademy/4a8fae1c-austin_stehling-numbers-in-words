
class Fixnum
  NUMBERS = {0 => "zero", 1 => "one", 2=> "two", 3 => "three", 4 => "four",
    5 => "five", 6 => "six", 7 => "seven", 8 => "eight", 9 => "nine", 10 => "ten",
    11 => "eleven", 12 => "twelve", 13 => "thirteen", 14 => "fourteen",
    15 => "fifteen", 16 => "sixteen", 17 => 'seventeen', 18 => "eighteen",
    19 => "nineteen", 20 => "twenty", 30 => "thirty", 40 => "forty", 50 => "fifty",
    60 => "sixty", 70 => "seventy", 80 => "eighty", 90 => "ninety"}

  def in_words
    case to_s.length
    when 1
      NUMBERS[self]
    when 2
      num = self.to_s
      NUMBERS.include?(self) ? NUMBERS[self] : "#{NUMBERS[num[0].to_i * 10]} #{NUMBERS[num[1].to_i]}"
    when 3
      if self % 100 == 0
        "#{NUMBERS[self/100]} hundred"
      else
        "#{NUMBERS[self/100]} hundred #{(self % 100).in_words}"
      end
    when 4..6
      if self % 1000 == 0
        "#{(self / 1000).in_words} thousand"
      else
        "#{(self / 1000).in_words} thousand #{(self % 1000).in_words}"
      end
    when 7..9
      if self % 1_000_000 == 0
        "#{(self / 1_000_000).in_words} million"
      else
        "#{(self / 1_000_000).in_words} million #{(self % 1_000_000).in_words}"
      end
    when 10..12
      if self % 1_000_000_000 == 0
        "#{(self / 1_000_000_000).in_words} billion"
      else
        "#{(self / 1_000_000_000).in_words} billion #{(self % 1_000_000_000).in_words}"
      end
    when 13..15
      if self % 1_000_000_000_000 == 0
        "#{(self / 1_000_000_000_000).in_words} trillion"
      else
        "#{(self / 1_000_000_000_000).in_words} trillion #{(self % 1_000_000_000_000).in_words}"
      end
    else raise "We don't have a conversion for #{self}"
    end

  end
end
